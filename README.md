# Comandos

## _Comandos básicos de GIT_

| Comando | Acción |
| ------ | ------ |
| git --version | Verificar si GIT esta instalado y/o la versión. |
| git help | Muestra una lista de los comandos mas usados en GIT y su función |
| git config --global user.name "nombre" | Para configurar el nombre que salen en los commits |
| git config --global user.email "email" | Para configurar el email |
| git clone <url> | Clonamos el repositorio de github o gitlab |
| git push | Para subir al repositorio |
| git init | Para iniciar GIT en la carpeta donde esta el proyecto |
| ls -la | Para mostrar la lista de archivos que están dentro del directorio/carpeta |
| echo >> "nombre de archivo"."extensión" | Crear un archivo |
| git status | Para comprobar el estado de los archivos |
| git add  | Para agregar los archivos a la zona de ensayo |
| git add -A | Para agregar TODOS los archivos a la zona de ensayo |
| git add . | Para agregar todo el directorio a la zona de ensayo |
| git commit -m "mensaje del commit" | Para enviar los archivos de la zona de ensayo al repositorio |
| git diff | Para mostrar los cambios realizados a un archivo |
| git log | Para visualizar el historial de cambios de tu repositorio |
| git checkout | Para desplazarte entre las ramas creadas y/o cambiar entre diferentes versiones de una entidad |
| git revert | Para deshacer cambios efectuados en el historial de confirmaciones de un repositorio |
| git reset | Para deshacer cambios |
| git remote | Para agregar repositorios remotos |
| git pull | Para buscar los cambios nuevos y actualizar el repositorio |
| git branch | Para crear, enumerar y eliminar ramas, así como cambiar su nombre |



## _Comandos básicos de LINUX_

| Comando | Acción |
| ------ | ------ |
| pwd | Para encontrar la ruta del directorio (carpeta) de trabajo actual en el que te encuentras |
| cd | Para navegar por los archivos y directorios |
| ls | Para ver el contenido de un directorio |
| cp | Para copiar archivos del directorio  actual a un directorio diferente |
| cat | Para ver el contenido de un archivo |
| mv | Para es mover archivos y/o cambiar el nombre de los archivos |
| echo | Para crear un archivo en blanco o agregar contenido a un archivo |
| mkdir | Para crear un directorio |
| rmdir | Para eliminar un directorio siempre y vcuando este vacío |
| rm | Para eliminar directorios y el contenido dentro de ellos |
| touch | Para crear un nuevo archivo en blanco a través de la línea de comando |
| locate | Para localizar un archivo |
| find | También para buscar archivos y directorios dentro de un directorio dado |
| grep | Para buscar a través de todo el texto en un archivo dado |
| sudo | Para realizar tareas que requieren permisos administrativos o raíz |
| df | Para obtener un informe sobre el uso del espacio en disco del sistema, se muestra en porcentaje y KB |
| du | Para verificar cuánto espacio ocupa un archivo o un directorio |
| head | Para ver las primeras líneas de cualquier archivo de texto |
| tail | Para mostrará las últimas diez líneas de un archivo de texto |
| diff | Para comparar el contenido de dos archivos línea por línea y mostrar las diferencias |
| tar | Para guardar múltiples archivos en un tarball o archivo comprimido |
| chmod | Para cambiar los permisos de lectura, escritura y ejecución de archivos y directorios |
| chown | Para cambiar o transferir la propiedad de un archivo al nombre de usuario especificado |
| jobs | Para mostrar todos los trabajos actuales junto con sus estados |
| kill | Para cerrar un programa que no responde |
| ping | para verificar tu estado de conectividad a una ip |
| vim, vi, nano | Para editar un archivo a través de la línea de comando o consola |
| su | Para cambiar de usuario |
| clear | Para limiar el texto en la terminal |
| exit | Para salir de la terminal |
